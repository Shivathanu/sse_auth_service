"use strict";
module.exports = {
	up: function(queryInterface, Sequelize) {
		return queryInterface.createTable("message", {
			id: {
				type: Sequelize.STRING,
				primaryKey: true,
				allowNull: false,
			},
			uuid: {
				type: Sequelize.STRING,
				allowNull: false,
				unique: true
			},
			email: {
				type: Sequelize.STRING,
				allowNull: false,
			},
			message: {
				type: Sequelize.STRING,
				allowNull: false,
			},
			hmac: {
				type: Sequelize.STRING,
			},
			signature: {
				type: Sequelize.TEXT,
			},
			create_time: {
				allowNull: false,
				defaultValue: new Date(),
				type: Sequelize.DATE,
			},
			update_time: {
				type: Sequelize.DATE,
			},
			delete_time: {
				type: Sequelize.DATE,
			},
		});
	},
	down: function(queryInterface, Sequelize) {
		return queryInterface.dropTable("message");
	}
};
