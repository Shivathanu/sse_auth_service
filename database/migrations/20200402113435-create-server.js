"use strict";
module.exports = {
	up: function(queryInterface, Sequelize) {
		return queryInterface.createTable("server", {
			pk_id: {
				type: Sequelize.INTEGER,
				primaryKey: true,
				allowNull: false,
				autoIncrement: true,
			},
			id: {
				type: Sequelize.STRING,
				allowNull: false,
			},
			uuid: {
				type: Sequelize.STRING,
				allowNull: false,
				unique: true
			},
			server_id: {
				type: Sequelize.STRING,
				allowNull: false,
			},
			public_key: {
				type: Sequelize.TEXT,
			},
			private_key: {
				type: Sequelize.TEXT,
			},
			create_time: {
				allowNull: false,
				defaultValue: new Date(),
				type: Sequelize.DATE,
			},
			update_time: {
				type: Sequelize.DATE,
			},
			delete_time: {
				type: Sequelize.DATE,
			},
		});
	},
	down: function(queryInterface, Sequelize) {
		return queryInterface.dropTable("server");
	}
};
