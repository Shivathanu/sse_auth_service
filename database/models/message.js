var models = require("../models");

/**
 * User Model
 */
module.exports = function(sequelize, DataTypes) {
	var Message = sequelize.define(
		"Message",
		{
			id: {
				allowNull: false,
				defaultValue: DataTypes.UUIDV4,
				type: DataTypes.UUID,
				primaryKey: true,
				unique: true,
			},
			uuid: {
				allowNull: false,
				defaultValue: DataTypes.UUIDV4,
				type: DataTypes.UUID,
				unique: true,
			},
			message: {
				type: DataTypes.STRING,
				allowNull: false,
			},
			email: {
				type: DataTypes.STRING,
				allowNull: false,
				references: {
					model: models.User,
					key: "email",
				},
			},
			hmac: {
				type: DataTypes.STRING
			},
			signature: {
				type: DataTypes.STRING
			}
		},
		{
			freezeTableName: true,
			tableName: "message",
			paranoid: true,
			createdAt: "create_time",
			updatedAt: "update_time",
			deletedAt: "delete_time",
		}
	);

	Message.associate = function (models) {
		models.Message.belongsTo(models.User, {
			as: "messageData",
			foreignKey: "email",
			foreignKeyConstraint: true,
		});
	};

	return Message;
};
