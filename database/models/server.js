/**
 * Server Model
 */
module.exports = function(sequelize, DataTypes) {
	var Server = sequelize.define(
		"Server",
		{
			pkId: {
				allowNull: false,
				primaryKey: true,
				autoIncrement: true,
				type: DataTypes.INTEGER,
				field: 'pk_id'
			},
			id: {
				allowNull: false,
				defaultValue: DataTypes.UUIDV4,
				type: DataTypes.UUID,
				unique: true,
			},
			uuid: {
				allowNull: false,
				defaultValue: DataTypes.UUIDV4,
				type: DataTypes.UUID,
				unique: true,
			},
			serverId: {
				type: DataTypes.STRING,
				allowNull: false,
				unique: true,
				field: 'server_id'
			},
			publicKey: {
				type: DataTypes.TEXT,
				allowNull: true,
				field: "public_key",
			},
			privateKey: {
				type: DataTypes.TEXT,
				allowNull: true,
				field: "private_key",
			}
		},
		{
			freezeTableName: true,
			tableName: "server",
			paranoid: true,
			createdAt: "create_time",
			updatedAt: "update_time",
			deletedAt: "delete_time"
		}
	);

	return Server;
};
