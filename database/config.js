const env = process.env.NODE_ENV || "development";

module.exports = {
	[env]: {
		database: "sse_auth_db",
		username: "sse_auth_user",
		password: "sse_auth_password",
		host: "34.67.226.171",
		dialect: "mysql",
		logging: !!process.env.DEBUG,
		migrationStorageTableName: "_migrations",
		pool: {
			max: 10,
			min: 1,
			acquire: 30000,
			idle: 10000,
		},
	},
};
