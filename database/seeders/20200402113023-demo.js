const usersData = require("../fixtures/users-data.json");
const serverData = require("../fixtures/server-data.json");

module.exports = {
	up: async queryInterface => {
		await queryInterface.bulkInsert("user", usersData);
		await queryInterface.bulkInsert("server", serverData);
	},

	down: async queryInterface => {
		await queryInterface.bulkDelete("user", usersData);
		await queryInterface.bulkDelete("server", serverData);
	}
};
