
function convertBufferToString(data) {
    return String.fromCharCode.apply(null, new Uint16Array(data));
};

module.exports = { convertBufferToString };
