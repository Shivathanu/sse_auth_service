const { publicEncrypt, privateDecrypt, constants } = require("crypto");
const utils = require('../utils/helper');
var security = {};
var logger = require("../config/log");

/**
 * Service layer to encrypt the information
 *
 * @param {Object} request
 * @param {Function} registerCB
 */
security.encrypt = function (publicKey, data, encryptCB) {
    try {
        // message to be encrypted
        var toEncrypt = data;
        var encryptBuffer = Buffer.from(toEncrypt, "utf8");

        // //encrypt using private key
        var encrypted = publicEncrypt(
            {
                key: publicKey,
                passphrase: "top secret",
                padding: constants.RSA_PKCS1_PADDING,
            },
            encryptBuffer
        );
        return encryptCB(null, encrypted.toString("base64"));
    } catch (err) {
        logger.error("Error occurred while encrypting", {
            error: err,
            params: data
        });
        return encryptCB('Invalid Key provided for encryption');
    }
}

/**
 * Service layer to decrypt the information
 *
 * @param {Object} request
 * @param {Function} registerCB
 */
security.decrypt = function(privateKey, encrypted, decryptCB) {
    if (encrypted === null || encrypted.length < 1) {
        return decryptCB('Invalid encrypt string');
    }
    try {
        // //decrypt the cyphertext using the public key
        var decryptBuffer = Buffer.from(encrypted, "base64");
        var decrypted = privateDecrypt(
            {
                key: privateKey,
                passphrase: "top secret",
                cipher: "aes-256-cbc",
                padding: constants.RSA_PKCS1_PADDING,
            },
            decryptBuffer
        );
        return decryptCB(null, JSON.parse(utils.convertBufferToString(decrypted)));
    } catch (err) {
        logger.error("Error occurred while decrypting", {
            error: err,
            params: encrypted
        });
        return decryptCB(err);
    }
}

module.exports = security;
