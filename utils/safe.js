const crypto = require("crypto");
const sharedSecret = 'I\'m a very hard random string';
var safe = {};
var _ = require('lodash');

safe.getHmac = function(sourceData, getHmacCB) {
	// The object is converted to a string because the update method
	// only accepts string, Buffer, TypedArray or DataView as types.
	const message = JSON.stringify(sourceData);

	// The crypto module provides cryptographic functionality that includes
	// a set of wrappers for OpenSSL's hash, HMAC, cipher, decipher, sign,
	// and verify functions.
	//
	// crypto.createHmac(algorithm, key[, options]): this method creates the HMAC.
	// In our example, we use the algorithm SHA256 that will combine the shared
	// secret with the input message and will return a hash digested as a base64
	// string.
	const hmac = crypto.createHmac('SHA256', sharedSecret)
		.update(message, 'utf-8')
		.digest('base64');

	console.log(`HMAC generated: ${hmac}`);

	return getHmacCB(null, hmac);
}

safe.validateHmac = function (hmac, sourceData, validateHmacCB) {
	const message = JSON.stringify(sourceData.message);
	if (_.isEmpty(hmac) || _.isEmpty(message)) {
		return validateHmacCB(null, false);
	}

	// Now we convert the hashes to Buffer because the timingSafeEqual needs them
	// as types Buffer, TypedArray or DataView
	const providedHmac = Buffer.from(hmac, 'utf-8');
	const generatedHash = Buffer.from(
		crypto.createHmac('SHA256', sharedSecret).update(message).digest('base64'),
		'utf-8',
	);

	// This method operates with secret data in a way that does not leak
	// information about that data through how long it takes to perform
	// the operation.
	// You could compare the hashes as string, but you should compare timing in
	// order to make your code safer.
	if (!crypto.timingSafeEqual(generatedHash, providedHmac)) {
		// The message was changed, the HMAC is invalid or the timing isn't safe
		return validateHmacCB(null, false);
	}
	return validateHmacCB(null, true);
}

safe.getSign = function (privateKey, data, getSignCB) {
	if(!privateKey) {
		return getSignCB(null, '');
	}
	var signature = crypto.createSign('RSA-SHA256').update(data).sign({
		key: privateKey,
		passphrase: 'top secret'
	}, 'base64');

	console.log(`Digital Signature: ${signature}`);

	return getSignCB(null, signature);
}

safe.verifySign = function (publicKey, signature, sourceData, verifySignCB) {
	// Create a verification sign

	if (_.isEmpty(publicKey) || _.isEmpty(signature)) {
		return verifySignCB(null, false);
	}
	const verifier = crypto.createVerify('RSA-SHA256');
	verifier.write(sourceData);
	verifier.end();

	// Match file signature with the public key against the signature provided
	const result = verifier.verify(publicKey, signature, 'base64');

	// Shows if it worked or not!
	console.log(`Digital Signature Verification: ${result}`);

	return verifySignCB(null, result);
}

module.exports = safe;
