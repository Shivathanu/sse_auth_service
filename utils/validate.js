var validator = {};

/**
 * Validate request body received during user registration
 *
 * @param {Object} request
 * @param {Object} response
 * @param {Function} next
 */
validator.register = function(request, response, next) {
    request.check('email', 'Email is required').notEmpty()
        .matches(/.+\@.+\..+/)
        .withMessage('Email must contain @');
    request.check('password', 'Password is required').notEmpty();
    const errors = request.validationErrors();
    if(errors) {
        return response.status(400).json({ error: errors.map(error => error.msg)[0] });
    }
    next();
};

/**
 * Validate request body received during user registration
 *
 * @param {Object} request
 * @param {Object} response
 * @param {Function} next
 */
validator.message = function(request, response, next) {
    request.check('message', 'Message cannot be empty').notEmpty()
    const errors = request.validationErrors();
    if(errors) {
        return response.status(400).json({ error: errors.map(error => error.msg)[0] });
    }
    next();
};

module.exports = validator;
