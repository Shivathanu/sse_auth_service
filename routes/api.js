var express = require("express");
var router = express.Router();

// Controller imports
var authController = require("../src/controllers/auth-controller");
var homeController = require("../src/controllers/home-controller");
var messageController = require("../src/controllers/message-controller");
var userController = require("../src/controllers/user-controller");
var serverController = require("../src/controllers/server-controller");

router.use("/auth", authController);
router.use("/", homeController);
router.use("/message", messageController);
router.use("/user", userController);
router.use("/server", serverController);

module.exports = router;
