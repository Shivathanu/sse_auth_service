var express = require("express");
var authController = express.Router();
var authService = require("../services/auth-service");
var logger = require("../../config/log");

/**
 * Controller method to encrypt the data
 *
 * @param {Object} request
 * @param {Object} response
 */
var encryptData = function (request, response) {
    authService.encryptData(request, request.body, function (error, result) {
        if (error) {
            logger.error('Error occurred while encrypting details', {
                error: error,
                params: request.body
            });
            response.status(500).send(error);
        }
        response.status(200).send(result);
    });
}

/**
 * Controller method to decrypt the data
 *
 * @param {Object} request
 * @param {Object} response
 */
var decryptData = function (request, response) {
    authService.decryptData(request, function (error, result) {
        if (error) {
            logger.error('Error occurred while decrypting details', {
                error: error,
                params: request.body
            });
            response.status(500).send(error);
        }
        response.status(200).send(result);
    });
}

/**
 * Controller method to decrypt the data
 *
 * @param {Object} request
 * @param {Object} response
 */
var verifyData = function (request, response) {
    authService.verifyData(request, function (error, result) {
        if (error) {
            logger.error('Error occurred while verifying details', {
                error: error,
                params: request.body
            });
            response.status(500).send(error);
        }
        response.status(200).send(result);
    });
}

authController.post("/encrypt", encryptData);

authController.post("/decrypt", decryptData);

authController.get("/verify", verifyData);

module.exports = authController;
