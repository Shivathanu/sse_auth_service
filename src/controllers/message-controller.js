var express = require("express");
var messageService = require("../services/message-service");
var logger = require("../../config/log");
var validator = require('../../utils/validate');
var messageController = express.Router();

/**
 * Controller method to send the message
 *
 * @param {Object} request
 * @param {Object} response
 */
var sendMessage = function (request, response) {
    messageService.sendMessage(request, function (error, result) {
        if (error) {
            logger.error('Error occurred while sending message', {
                error: error,
                params: request.body
            });
            response.status(500).send(error);
        }
        response.status(200).send(result);
    });
}

/**
 * Controller method to verify the message
 *
 * @param {Object} request
 * @param {Object} response
 */
var verifyMessage = function (request, response) {
    messageService.verifyMessage(request, function (error, result) {
        if (error) {
            logger.error('Error occurred while verifying message', {
                error: error,
                params: request.body
            });
            response.status(500).send(error);
        }
        response.status(200).send(result);
    });
}

messageController.post("/", validator.message, sendMessage);

messageController.get("/verify", validator.message, verifyMessage);

module.exports = messageController;
