var express = require("express");
var serverController = express.Router();
var logger = require("../../config/log");
var serverService = require('../services/server-service');
var security = require("../../utils/security");

/**
 * Controller method to register a server
 *
 * @param {Object} request
 * @param {Object} response
 */
var registerServer = function (request, response) {
    serverService.register(request, function (error, result) {
        if (error) {
            logger.error('Error occurred while registering server', {
                error: error,
                params: request.body
            });
            response.status(500).json(error);
        }
        response.status(200).json(result);
    });
}

/**
 * Controller method to get the server details
 *
 * @param {Object} request
 * @param {Object} response
 */
var getServerDetails = function (request, response) {
    serverService.getDetails(request, function (error, result) {
        if (error) {
            logger.error('Error occurred while getting the server', {
                error: error,
                params: request.body
            });
            response.status(500).json(error);
        }
        response.status(200).json(result);
    });
}


serverController.post("/generateKeyPair", registerServer);

serverController.get("/", getServerDetails);

module.exports = serverController;
