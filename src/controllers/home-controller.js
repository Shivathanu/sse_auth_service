var express = require("express");
var homeController = express.Router();
/**
 * Controller method to verify a user
 *
 * @param {Object} request
 * @param {Object} response
 */
var renderHomePage = function (request, response) {
    response.render("home", {
        getUsername: function () {
            return 'Test User';
        }
    });
}

homeController.get("/", renderHomePage);

module.exports = homeController;
