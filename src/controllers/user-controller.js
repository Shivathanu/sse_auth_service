var express = require("express");
var logger = require("../../config/log");
var userController = express.Router();
var userService = require("../services/user-service");
var validator = require('../../utils/validate');

/**
 * Controller method to register a user
 *
 * @param {Object} request
 * @param {Object} response
 */
var registerUser = function (request, response) {
    userService.register(request, function (error, result) {
        if (error) {
            logger.error('Error occurred while registering user', {
                error: error,
                params: request.body
            });
            response.status(500).json(error);
        }
        response.status(200).json(result);
    });
}

userController.post("/register", validator.register, registerUser);

module.exports = userController;
