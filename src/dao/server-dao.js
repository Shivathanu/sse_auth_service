var models = require("../../database/models/index");
const { errorHandler } = require("../../utils/dbErrorHandler");
var serverDao = {};

/**
 * Data Access Object that registers a user
 *
 * @param {Object} request
 * @param {Function} registerCB
 */
serverDao.register = function (request, registerCB) {
	models.Server.create(request.body).then(
		function (register) {
			return registerCB(null, register);
		},
		function (error) {
			return registerCB({
				error: errorHandler(error)
			});
		}
	);
};

/**
 * Data Access Object that performs the data fetch for one user in the system
 *
 * @param {Object} request
 * @param {Function} getOneCB
 */
serverDao.getOne = function (request, check, getOneCB) {
	var whereParam;
	if(check === 'PK') {
		whereParam = {
			pkId: 1
		};
	}
	if(check === 'SERVERID') {
		whereParam = {
			serverId: request.body.serverId || request.params.serverId || request.query.serverId
		};
	}
	models.Server.findOne({
		where: whereParam,
		attributes: ["serverId", "privateKey", "publicKey"]
	}).then(
		function (user) {
			return getOneCB(null, user);
		},
		function (error) {
			return getOneCB({
				error: error,
				response: error.parent,
			});
		}
	);
};

module.exports = serverDao;
