var bcrypt = require("bcrypt");
var models = require("../../database/models/index");
const { errorHandler } = require("../../utils/dbErrorHandler");
var messageDao = {};

/**
 * Data Access Object that creates a user
 *
 * @param {Object} request
 * @param {Function} createCB
 */
messageDao.create = function (request, createCB) {
    models.Message.create(request.body, { returning: true }).then(
        function (create) {
            return createCB(null, create);
        },
        function (error) {
            return createCB({
                error: errorHandler(error),
            });
        }
    );
};

/**
 * Data Access Object that updates a message
 */
messageDao.update = function (request, updateCB) {
    models.Message.update(request.body, {
        where: {
            uuid: request.body.uuid,
        },
    }).then(
        function (response) {
            return updateCB(null, response);
        },
        function (error) {
            return updateCB({
                error: error,
                response: error.parent,
            });
        }
    );
};

/**
 * Data Access Object that performs the data fetch for one message in the system
 *
 * @param {Object} request
 * @param {Function} getOneCB
 */
messageDao.getOne = function (request, getOneCB) {
    models.Message.findOne({
        where: {
            uuid: request.params.messageId || request.body.messageId || request.query.messageId,
            message: request.params.message || request.body.message || request.query.message,
            hmac: request.params.hmac || request.body.hmac || request.query.hmac,
        },
        attributes: {
            exclude: ["create_time", "update_time", "delete_time"],
        },
    }).then(
        function (message) {
            return getOneCB(null, message);
        },
        function (error) {
            return getOneCB({
                error: error,
                response: error.parent,
            });
        }
    );
};

module.exports = messageDao;
