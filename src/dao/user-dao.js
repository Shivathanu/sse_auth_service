var bcrypt = require("bcrypt");
var models = require("../../database/models/index");
const { errorHandler } = require("../../utils/dbErrorHandler");
var _ = require('lodash');
var userDao = {};

/**
 * Data Access Object that registers a user
 *
 * @param {Object} request
 * @param {Function} registerCB
 */
userDao.register = function (request, registerCB) {
	models.User.create(request.body, { returning: true }).then(
		function (register) {
			return registerCB(null, register);
		},
		function (error) {
			return registerCB({
				error: errorHandler(error),
			});
		}
	);
};

/**
 * Data Access Object that performs the data fetch for one user in the system
 *
 * @param {Object} request
 * @param {Function} getKeysCB
 */
userDao.getKeys = function (request, getKeysCB) {
	models.User.findOne({
		where: {
			email: request.params.email || request.body.email || request.query.email || 'xx',
		},
		attributes: ["privateKey", "publicKey"]
	}).then(
		function (user) {
			return getKeysCB(null, user);
		},
		function (error) {
			return getKeysCB({
				error: error,
				response: error.parent,
			});
		}
	);
};

/**
 * Data Access Object that performs the data fetch for one user in the system
 *
 * @param {Object} request
 * @param {Function} getOneCB
 */
userDao.getOne = function (request, getOneCB) {
	models.User.findOne({
		where: {
			email: request.params.email || request.body.email || request.query.email
		},
		attributes: {
			exclude: ["privateKey", "publicKey", "create_time", "update_time", "delete_time"],
		},
	}).then(
		function (user) {
			user = _.get(user, 'password') && (bcrypt.compareSync(
					request.body.password, 
					user.password)
				) ? user : null;
			return getOneCB(null, user);
		},
		function (error) {
			return getOneCB({
				error: error,
				response: error.parent,
			});
		}
	);
};

module.exports = userDao;
