var async = require("async");
var _ = require("lodash");
var serverDao = require("../dao/server-dao");
var authService = require('./auth-service');
var moment = require('moment');
var serverService = {};

/**
 * Service layer to call the Data Access Object (DAO) for database operations
 *
 * @param {Object} request
 * @param {Function} registerCB
 */
serverService.register = function (request, registerCB) {
	async.waterfall([
		async.apply(authService.generateKeys, request),
		function (result, passCB) {
            request.body = {
                ...request.body, 
                ...result, 
				serverId: 'SES-' + moment().format('DDMMYYYYHHMMSS')
			};
			return passCB(null, request);
		},
		serverDao.register
	], function (waterFallErr) {
		if (waterFallErr) {
			return registerCB(waterFallErr);
		}
		return registerCB(null, { response: `server: ${request.body.serverId} registered successfully` });
	});
};

/**
 * Service layer to call the Data Access Object (DAO) for database operations
 *
 * @param {Object} request
 * @param {Function} getDetailsCB
 */
serverService.getDetails = function (request, getDetailsCB) {
	serverDao.getOne(request, 'PK', function(error, result) {
		if(error) {
			return getDetailsCB(error);
		}
		return getDetailsCB(null, result);
	});
};

module.exports = serverService;
