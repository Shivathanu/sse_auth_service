var async = require('async');
var security = require("../../utils/security");
var companyDao = require("../dao/company-dao");
var utilService = {};

/**
 * Service layer to encrypt the data using company key
 *
 * @param {Object} request
 * @param {Function} createCB
 */
utilService.encryptData = function (request, data, encryptCB) {
	async.waterfall([
        async.apply(companyDao.getOne, request),
        function (result, passCB) {
            return passCB(null, result.privateKey, JSON.stringify(data));
        },
        security.encrypt
    ], function (waterFallErr, result) {
        if (waterFallErr) {
            return encryptCB(waterFallErr);
        }
        return encryptCB(null, result);
    });
};

/**
 * Service layer to encrypt the data using company key
 *
 * @param {Object} request
 * @param {Function} createCB
 */
utilService.decryptData = function (request, data, decryptCB) {
	async.waterfall([
        async.apply(companyDao.getOne, request),
        function (result, passCB) {
            return passCB(null, result.publicKey, data);
        },
        security.decrypt
    ], function (waterFallErr, result) {
        if (waterFallErr) {
            return decryptCB(waterFallErr);
        }
        return decryptCB(null, result);
    });
};

module.exports = utilService;
