var async = require("async");
var _ = require("lodash");
var messageDao = require("../dao/message-dao");
var userDao = require("../dao/user-dao");
var safe = require('../../utils/safe');
var messageService = {};

/**
 * Service layer to get the data for user & message verification
 *
 * @param {Object} request
 * @param {Function} verifyUserCB
 */
messageService.getData = function (request, verifyDataCB) {
    async.parallel({
        user: userDao.getKeys.bind(null, request),
        message: messageDao.getOne.bind(null, request),
    }, function (parallelErr, parallelResult) {
        if (parallelErr) {
            return verifyDataCB(parallelErr);
        }
        return verifyDataCB(null, parallelResult);
    });
};

/**
 * Service layer to call the Data Access Object (DAO)
 * to update a message.
 *
 * @param {Object} request
 * @param {Function} updateCB
 */
messageService.updateData = function (request, updateCB) {
    messageDao.update(request, function (error, result) {
        if (error) {
            return updateCB(error);
        }
        return updateCB(null, result);
    });
};

/**
 * Service layer to call the Data Access Object (DAO) for database operations
 *
 * @param {Object} request
 * @param {Function} createCB
 */
messageService.sendMessage = function (request, createCB) {
    var updateMessage = {};
    var messageData;
    async.waterfall([
        async.apply(messageDao.create, request),
        function (result, passCB) {
            messageData = result;
            return passCB(null, result.message);
        },
        safe.getHmac,
        function (hmac, passCB) {
            request.body = { ...request.body, hmac, uuid: messageData.uuid };
            updateMessage.hmac = hmac;
            updateMessage.uuid = request.body.uuid;
            return passCB(null, request);
        },
        userDao.getKeys,
        function (result, passCB) {
            var privateKey;
            if(!result) {
                privateKey = '';
            } else {
                privateKey = result.privateKey;
            }
            return passCB(null, privateKey, request.body.message);
        },
        safe.getSign,
        function (result, passCB) {
            request.body = { ...request.body, signature: result};
            return passCB(null, request);
        },
        messageService.updateData
    ], function (waterFallErr) {
        if (waterFallErr) {
            return createCB(waterFallErr);
        }
        updateMessage.response = 'Message sent successfully';
        return createCB(null, updateMessage);
    });
};

/**
 * Service layer to authenticate the user
 *
 * @param {Object} request
 * @param {Function} verifyUserCB
 */
messageService.verifyMessage = function (request, verifyUserCB) {
    var messageBody = {};
    var messageData;
    let hmacValue = ''
    var isHmacValid;
    var isSignValid;
    var response = {};
    async.waterfall([
        async.apply(messageService.getData, request),
        function (result, passCB) {
            messageData = result;
            if (result.message) {
                messageBody.message = result.message.message;
                hmacValue = result.message.hmac;
            } else {
                messageBody.message = '';
            }
            return passCB(null, hmacValue, messageBody);
        },
        safe.validateHmac,
        function (result, passCB) {
            isHmacValid = result;
            if (!messageData.message) {
                messageData.message = { signature: '' };
            }
            if (!messageData.user) {
                messageData.user = { publicKey: ''};
            }
            return passCB(null,
                messageData.user.publicKey,
                messageData.message.signature,
                request.query.message
            );
        },
        safe.verifySign,
        function (result, passCB) {
            isSignValid = result;
            if (isHmacValid && !isSignValid) {
                response.response = 'Only Hmac is valid';
            }
            if (!isHmacValid && isSignValid) {
                response.response = 'Only Signature is valid';
            }
            if (isHmacValid && isSignValid) {
                response.response = 'Valid Signature and Hmac';
            }
            if (!isHmacValid && !isSignValid) {
                response.response = 'Invalid Signature and Hmac';
            }
            return passCB(null, response);
        }
    ], function (waterFallErr, result) {
        if (waterFallErr) {
            return verifyUserCB(waterFallErr);
        }
        return verifyUserCB(null, result);
    });
};

module.exports = messageService;
