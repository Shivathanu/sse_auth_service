var async = require("async");
var _ = require("lodash");
var userDao = require("../dao/user-dao");
var serverDao = require("../dao/server-dao");
var authService = require('./auth-service');
var userService = {};

/**
 * Service layer to call the Data Access Object (DAO) for database operations
 *
 * @param {Object} request
 * @param {Function} registerCB
 */
userService.register = function (request, registerCB) {
	var response = {};
	async.waterfall([
		async.apply(authService.generateKeys, request),
		function (result, passCB) {
			request.body = { ...request.body, ...result };
			return passCB(null, request);
		},
		userDao.register,
		function (result, passCB) {
			response.userPrivateKey = result.privateKey;
			return passCB(null, request, 'PK');
		},
		serverDao.getOne,
		function (result, passCB) {
			response.serverPublicKey = result.publicKey;
			return passCB(null, request);
		}
	], function (waterFallErr) {
		if (waterFallErr) {
			return registerCB(waterFallErr);
		}
		response.response = `user: ${request.body.email} registered successfully`;
		return registerCB(null, response);
	});
};

module.exports = userService;
