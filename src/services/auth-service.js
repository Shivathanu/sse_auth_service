var authService = {};
var async = require('async');
var uuid = require("uuid");
var serverDao = require("../dao/server-dao");
var userDao = require("../dao/user-dao");
var security = require("../../utils/security");
var _ = require("lodash");

/**
 * Service layer to encrypt the data using public key
 *
 * @param {Object} request
 * @param {Object} data
 * @param {Function} createCB
 */
authService.encryptData = function (request, data, encryptCB) {
	async.waterfall([
        async.apply(serverDao.getOne, 'PK', request),
        function (result, passCB) {
            data = _.omit(data, 'publicKey');
            return passCB(null, result.publicKey, JSON.stringify(data));
        },
        security.encrypt
    ], function (waterFallErr, result) {
        if (waterFallErr) {
            return encryptCB(waterFallErr);
        }
        return encryptCB(null, result);
    });
};

/**
 * Service layer to encrypt the data using private key
 *
 * @param {Object} request
 * @param {String} data
 * @param {Function} decryptCB
 */
authService.decryptData = function (request, data, decryptCB) {
	async.waterfall([
        async.apply(serverDao.getOne, 'PK', request),
        function (result, passCB) {
            return passCB(null, result.privateKey, data);
        },
        security.decrypt
    ], function (waterFallErr, result) {
        if (waterFallErr) {
            return decryptCB(waterFallErr);
        }
        return decryptCB(null, result);
    });
};

/**
 * Service layer to get the data for user & server verification
 *
 * @param {Object} request
 * @param {Function} getDataCB
 */
authService.getData = function (request, getDataCB) {
    if(!_.get(request.body, 'email')) {
        return getDataCB(null, '');
    }
    if(!_.get(request.body, 'password')) {
        return getDataCB(null, '');
    }
    async.parallel({
        user: userDao.getOne.bind(null, request),
        server: serverDao.getOne.bind(null, request, 'SERVERID'),
    }, function (parallelErr, parallelResult) {
        if (parallelErr) {
            return getDataCB(parallelErr);
        }
        return getDataCB(null, parallelResult);
    });
};

/**
 * Service layer to get the data for user & server verification
 *
 * @param {Object} request
 * @param {Function} verifyDetailsCB
 */
authService.verifyDetails = function (data, verifyDetailsCB) {
    if (data.user && data.server) {
        return verifyDetailsCB(null, { response: "Valid user & server details" });
    } 
    if (data.user && !data.server) {
        return verifyDetailsCB(null, { response: "Invalid server details" });
    }
    if (!data.user && data.server) {
        return verifyDetailsCB(null, { response: "Invalid user details" });
    }
    if (!data.user && !data.server) {
        return verifyDetailsCB(null, { response: "Invalid user & server details" });
    }
};

/**
 * Service layer to authenticate the user
 *
 * @param {Object} request
 * @param {Function} verifyDataCB
 */
authService.verifyData = function (request, verifyDataCB) {
	async.waterfall([
		async.apply(authService.decryptData, request, request.query.data),
        function(result, passCB) {
            request.body = { ...request.body, ...result };
            return passCB(null, request);
        },
        authService.getData,
        authService.verifyDetails
	], function (waterFallErr, result) {
		if (waterFallErr) {
			return verifyDataCB(waterFallErr);
		}
		return verifyDataCB(null, result);
	});
}

/**
 * Controller method to create the public and private key pair
 *
 * @param {Object} request
 * @param {Object} authCB
 */
authService.generateKeyPair = function (request, authCB) {
    const { generateKeyPair } = require("crypto");
    generateKeyPair(
        "rsa",
        {
            modulusLength: 2048,
            publicKeyEncoding: {
                type: "spki",
                format: "pem",
            },
            privateKeyEncoding: {
                type: "pkcs8",
                format: "pem",
                cipher: "aes-256-cbc",
                passphrase: "top secret",
            },
        },
        (err, publicKey, privateKey) => {
            if (err) {
                return authCB(err);
            }
            return authCB(null, { publicKey, privateKey, uuid: uuid.v4() });
        }
    );
};

/**
 * Controller method to create the keys
 *
 * @param {Object} request
 * @param {Object} authCB
 */
authService.generateKeys = function(request, authCB) {
    authService.generateKeyPair(request, function(error, result) {
        if(error) {
            return authCB(error);
        }
        return authCB(null, result);
    })
}

module.exports = authService;
