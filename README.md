# Secure transfer message Service

Service to handle the data required for the security transfer application

Application URL: [https://sse-auth-service.herokuapp.com/](https://sse-auth-service.herokuapp.com/)

## Tools Used

- NodeJS

## Framework

- Express

For more details about [Express](https://expressjs.com/)

- Sequelize

Sequelize is a promise-based Node.js ORM for Postgres, MySQL, MariaDB, SQLite and Microsoft SQL Server. It features solid transaction support, relations, eager and lazy loading, read replication and more.

For more details about [Sequelize](https://sequelize.org/)

## Installation

Use the npm manager [npm](https://www.npmjs.com/get-npm) to install project library dependencies.

1. Install Express into your application folder.
Refer [Express Installation](https://expressjs.com/en/starter/installing.html)
for initial setup

2. Install the sequelize dependencies.
```bash
yarn add sequelize
yarn add sequelize-cli
```

3. Install Sequelize driver dependencies.
```bash
yarn add tedious
```
Refer [Sequelize Installation](https://sequelize.org/) for setup

4. Database Migration
```bash
yarn db:all
```

5. For running the project locally
```bash
yarn run start
```

## Project Folder Structure

```bash
.
├── bin
    ├──www        #bin/ directory contains startup scripts and www is an example to start the server
├── config        #Contains configuration related files
├── database
    ├── migrations    #Contains Database schemas
    ├── models        #Object Model information
    ├── fixtures      #Test data to be loaded into database
    ├── seeders       #Seed script to load the fixtures
├── doc           #Documents related to services (Postman collection)
├── log           #Log informations
├── public        #Contain static content that server can render / serve to web application
├── views         #Contain web display content
├── routes        #Handle the routes to various endpoints
├── src
    ├── controllers #Serves main routes that is starting point for the api execution
    ├── dao        #Data Access Object Layer to handle the database related operations
    ├── services   #Process the api requests and manipulate the data received from dao layer
├── utils          #Data Access Object Layer to handle the database related operations
├── .gitignore     #Ignores specified directory / files
├── .gitlab-ci.yml #ci / cd script
├── app.js         #Application entry point
├── package.json   #Contains project dependency modules
├── README.md
├── yarn.lock      #Contains package dependency versions
```
